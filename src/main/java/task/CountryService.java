
package task;

import java.util.List;

public interface CountryService {
    List<Country>list();
    Country listId(int id);
    Country add(Country p);
    Country edit(Country p);
    Country delete(int id);
}
