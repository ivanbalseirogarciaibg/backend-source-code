
package task;


import java.util.List;
import org.springframework.data.repository.Repository;

public interface CountryRepository extends Repository<Country, Integer>{
    List<Country>findAll();
    Country findOne(int id);
    Country save(Country p);
    void delete(Country p);
}
