
package task;

import javax.persistence.*;

@Entity
@Table(name = "country")
public class Country {
    
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column
    private String name;
    @Column 
    private int population;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

	public int getPopulation() {
		return population;
	}

	public void setPopulation(int population) {
		this.population = population;
	}


    
    
}
