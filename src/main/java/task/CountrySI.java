
package task;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CountrySI implements CountryService{
    @Autowired
    private CountryRepository repositorio;
    
    @Override
    public List<Country> list() {
        return repositorio.findAll();
    }

    @Override
    public Country listId(int id) {
        return repositorio.findOne(id);
    }

    @Override
    public Country add(Country p) {
        return repositorio.save(p);
    }

    @Override
    public Country edit(Country p) {
        return repositorio.save(p);
    }

    @Override
    public Country delete(int id) {
        Country p=repositorio.findOne(id);
        if(p!=null){
            repositorio.delete(p);
        }
       return p;
    }
    
    
    
}
