
package task;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:4200",maxAge = 3600)
@RestController
@RequestMapping({"/countries"})
public class Controller {
    
    @Autowired
    CountryService service;
    
    @GetMapping
    public List<Country>list(){
        return service.list();
    }
    @PostMapping
    public Country agregar(@RequestBody Country c){
        return service.add(c);
    }
    @GetMapping(path = {"/{id}"})
    public Country listarId(@PathVariable("id")int id){
        return service.listId(id);
    }
    @PutMapping(path = {"/{id}"})
    public Country editar(@RequestBody Country c,@PathVariable("id") int id){
        c.setId(id);
        return service.edit(c);
    }
    @DeleteMapping(path = {"/{id}"})
    public Country delete(@PathVariable("id") int  id){
        return service.delete(id);
    }
}
