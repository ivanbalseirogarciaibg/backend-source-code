Instructions:
1)	Install and execute the IDE (recommended Spring Tool Suite 4)
2)	Import maven project
3)	Select the root of the pom.xml file
4)	Right click on the project and run as spring boot project





Controller.java:

Rest controller class is in charge of providing the angular code methods to provide the full functionality of the GUI.

Country.java:

Entity class that contains the information of each country: the id, the name and the population.

CountryRepository.java:

Interface in charge of having the methods to ask the database for the information

CountryService.java:

Interface that contains the methods to handle the operations with countries: add, delete, list, update and search for ID.

CountrySI.java:

Class that implements the above and overrides the functionalities with a CountryRepository to implement the methods.

TaskApplication.java:

Class that contains the main execution of the program

application.properties:

File that contains the information of the database and the server context path that is part of the url when different from the current “/”.

pom.xml:

File that contains the maven dependencies of the project
